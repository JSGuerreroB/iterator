/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.Conjunto;

/**
 *
 * @author SGuerrero
 */
public class TestConjunto {
    public static void main(String[] args) {
     Conjunto<Integer> c = new Conjunto<>();
     c.insertar(1);
     c.insertar(2);
     c.insertar(3);
     Conjunto<Integer> b = new Conjunto<>();
     b.insertar(3);
     b.insertar(6);
     b.insertar(8);
     Conjunto<Integer> union = c.getUnion(b);
        System.out.println(union);
     Conjunto<Integer> interseccion = c.getInterseccion(b);
     System.out.println(interseccion);
     Conjunto<Integer> c1 = new Conjunto<>();
     c1.insertar(1);
     c1.insertar(2);
     c1.insertar(3);
     Conjunto<Integer> b1 = new Conjunto<>();
     b1.insertar(3);
     b1.insertar(6);
     b1.insertar(8);
     Conjunto<Integer> diferencia = c1.getDiferencia(b1);
        System.out.println(diferencia);
     Conjunto<Integer> diferenciaA = c.getDiferenciaAsimetrica(b);
        System.out.println(diferenciaA);
     Conjunto<Conjunto<Integer>> potenciaC = c1.getPotencia();
        System.out.println(potenciaC);
    }
}
