/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util.seed;

/**
 *  Wrapper (Envoltorio)
 * @author docente
 * @param <T>
 */
public class Conjunto<T> implements IConjunto<T>{
    
    private ListaS<T> elementos;

    public Conjunto() {
        elementos = new ListaS();
    }
    
    @Override
    public Conjunto<T> getUnion(Conjunto<T> c1) {
        Conjunto<T> c= new Conjunto<T>();
        c.elementos.addAll(this.elementos);
        c.elementos.addAll(c1.elementos);
        return c;
    }

    @Override
    public Conjunto<T> getInterseccion(Conjunto<T> c1) {
        Conjunto<T> out= new Conjunto<T>();
        for (T info : c1.elementos) {
            if(c1.contains(info)){
                out.insertar(info);
            }
        }
        return out;
    }

    @Override
    public Conjunto<T> getDiferencia(Conjunto<T> c1) {
        Conjunto<T> out= new Conjunto<T>();
        for (T info : c1.elementos) {
            if(!this.contains(info)){
                out.insertar(info);
            }
        }
        return out;
    }

    @Override
    public Conjunto<T> getDiferenciaAsimetrica(Conjunto<T> c1) {
        Conjunto<T> out= new Conjunto<T>();
        for (T info : c1.elementos) {
            if(!this.contains(info)){
                out.insertar(info);
            }
        }
        for (T info : this.elementos) {
            if(!c1.contains(info)){
                out.insertar(info);
            }
        }
        return out;
    }

    @Override
    public Conjunto<Conjunto<T>> getPares(Conjunto<T> c1) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public T get(int i) {
        if(this.elementos==null)throw new RuntimeException("El conjunto no esta creado no se puede realizar la operacion");
        if(this.isVacia()) throw new RuntimeException("El conjunto esta vacio no se puede realizar la operacion");
        return this.elementos.get(i);
    }

    @Override
    public void set(int i, T info) {
                    if(this.elementos==null)throw new RuntimeException("El conjunto no esta creado no se puede realizar la operacion");
                    this.elementos.set(i, info);
                }

    @Override
    public void insertar(T info) {
        if(this.elementos==null)throw new RuntimeException("El conjunto no esta creado no se puede realizar la operacion");
        this.elementos.insertarInicio(info);
    }

    @Override
    public boolean isVacia() {
        return this.elementos.isVacia();
    }

    @Override
    public Conjunto<Conjunto<T>> getPotencia() {
            Conjunto<Conjunto<T>> subConjuntos= new Conjunto<Conjunto<T>>();
            //1 2 3 4
            //{},{1},{1,2},{1,3},{1,2,3},{2},{2,3},{3} 
            //
            subConjuntos.insertar(this);
            for (int i = 0; i < this.elementos.getSize(); i++) {    
            Conjunto<T> solo = new Conjunto<T>();
            solo.insertar(this.elementos.get(i));
            subConjuntos.insertar(solo);    
                for (int j = i+1; j < this.elementos.getSize(); j++) {
                    Conjunto<T> par = new Conjunto<T>();
                    par.insertar(this.elementos.get(i));
                    par.insertar(this.elementos.get(j));
                    subConjuntos.insertar(par);
                }
            }
            Conjunto<T> vacio = new Conjunto<T>();
            subConjuntos.insertar(vacio);  
              
            return subConjuntos;
    }
    
    
    public boolean contains(T info){
        for (T in : this.elementos) {
            if(in.equals(info)){
                return true;
            }
        }   
    return false;
    }
    
    @Override
    public String toString(){
        String out=""; 
        for (int i = 0; i < this.elementos.getSize(); i++) {
            if(i!=this.elementos.getSize()-1){
                out += this.elementos.get(i)+",";            
            }else{
                out += this.elementos.get(i);    
            }
        }
        return "{"+out+"}";
    }
    
}
