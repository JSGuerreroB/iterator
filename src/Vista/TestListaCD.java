/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.ListaCD;
import java.util.Iterator;
import java.util.Random;

/**
 *
 * @author DOCENTE
 */
public class TestListaCD {

    public static void main(String[] args) {
        ListaCD<Integer> l = crearInicio(10);
        ListaCD<Integer> l1 = crearFin(10);
        System.out.println(l);
        System.out.println(l1);
        //l1.remove(5);
        /*
        for (Integer i : l1) {
            System.out.print(i + ", ");
        }
        System.out.println("");
        Iterator<Integer> it = l1.iterator();
        while (it.hasNext()) {
            System.out.print(it.next() + ", ");
        }
        System.out.println("");
        ListaCD<Integer> intersec = l.getInterseccion(l1);
        System.out.println(intersec);
        */
        l.concat(l1, 5);
        System.out.println(l);
        System.out.println(l1);
        ListaCD<Integer> l2 = new ListaCD<Integer>();       
        l2.insertarFin(1);
        l2.insertarFin(2);
        l2.insertarFin(4);
        l2.insertarFin(6);
        l2.insertarFin(7);
        l2.insertarFin(8);
        ListaCD<Integer> l3 = new ListaCD<Integer>();       
        l3.insertarFin(1);
        l3.insertarFin(3);
        l3.insertarFin(5);
        l3.insertarFin(7);
        System.out.println(l2);
        System.out.println(l3);
        System.out.println(l2.getInterseccionOrdenado(l3));
    }

    private static ListaCD<Integer> crearInicio(int n) {

        if (n <= 0) {
            throw new RuntimeException("No puedo crear lista");
        }
        ListaCD<Integer> l = new ListaCD();
        while (n > 0) {
            l.insertarInicio(new Random().nextInt(n));
            n--;
        }
        return l;
    }

    private static ListaCD<Integer> crearFin(int n) {

        if (n <= 0) {
            throw new RuntimeException("No puedo crear lista");
        }
        ListaCD<Integer> l = new ListaCD();
        int i = 1;
        while (i <= n) {
            l.insertarFin(i);
            i++;
        }
        return l;
    }
}
