/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;
import Util.seed.Pila;
import java.util.Scanner;
/**
 *
 * @author DOCENTE
 */
public class TestValidador_Agrupamiento {
    public static void main(String[] args) {
        System.out.println("Validando [{()}] ");
        /**
         * Ejemplo: L={aa} => --> V
         * L={[aa]} => --> F
         * L=(a)[a]{b}--> V
         */
        Scanner t = new Scanner(System.in);
        String s = t.nextLine();
        Pila<Character> sig = new Pila<Character>();
        boolean abreC = true;
        boolean abreL = true;
        boolean abreP = true;
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i)=='['&&abreC){
                sig.push(s.charAt(i));
                abreC=false;
            }else if(s.charAt(i)==']'&&!abreC){
                sig.pop();
                abreC=true;
            }else if(!abreC){
                if(s.charAt(i)=='{'&&abreL){
                sig.push(s.charAt(i));
                abreL=false;
                }else if(s.charAt(i)=='}'&&!abreL){
                sig.pop();
                abreL=true;
                }else if(!abreL){
                    if(s.charAt(i)=='('&&abreP){
                    sig.push(s.charAt(i));
                    abreP=false;
                    }else if(s.charAt(i)==')'&&!abreP){
                    sig.pop(); 
                    abreP=false;
                    }
                }
            }
        }
        if(sig.size()==0){
            System.out.println("cumple");
        }else{
            System.out.println("No cumple");
        }
        System.out.println(sig.size());
    }
    
    
    
    
}
